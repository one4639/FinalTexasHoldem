﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texas_Holdem.Library.Enums
{
    public enum Suits
    {
        Hearts = '\u2665',
        Clubs = '\u2660',
        Diamonds = '\u2666',
        Spaders = '\u2663',
        Unknown = ' '
    }   
}
