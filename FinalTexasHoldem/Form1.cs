﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texas_Holdem.Library.Classes;
using Texas_Holdem.Library.Enums;
using Texas_Holdem.Library.Structs;

// Creating a table page 9

namespace FinalTexasHoldem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Label> _playerCardLabels = new List<Label>();
        List<Label> _dealerCardLabels = new List<Label>();

        Point[] _playerLblPos = new Point[]
        {
            new Point(630, 265), new Point(680, 265),
            new Point(500, 365), new Point(550, 365),
            new Point(300, 365), new Point(350, 365),
            new Point(160, 265), new Point(210, 265)
        };
        Point[] _dealerLblPos = new Point[]
        {
            new Point(320, 265), new Point(370, 265),
            new Point(420, 265), new Point(470, 265),
            new Point(520, 265)
        };

        private void Form1_Load(object sender, EventArgs e)
        {

            

            try
            {

                #region Deckinstance, shulle and var card.
                Deck DeckInstance = new Deck();
                DeckInstance.ShuffleDeck(9);
                var card = DeckInstance.DrawCard();
                #endregion

                #region testcard
                //var card = new Card(Texas_Holdem.Library.Enums.Values.Four, Texas_Holdem.Library.Enums.Suits.Clubs);

                //_playerCardLabels.Add(CreateCard(_playerLblPos[0].X, _playerLblPos[0].Y, card));
                //_playerCardLabels.Add(CreateCard(_playerLblPos[1].X, _playerLblPos[1].Y, card));
                //_playerCardLabels.Add(CreateCard(_playerLblPos[2].X, _playerLblPos[2].Y, card));
                //_playerCardLabels.Add(CreateCard(_playerLblPos[3].X, _playerLblPos[3].Y, card));
                //_playerCardLabels.Add(CreateCard(_playerLblPos[4].X, _playerLblPos[4].Y, card));
                //_playerCardLabels.Add(CreateCard(_playerLblPos[5].X, _playerLblPos[5].Y, card));
                //_playerCardLabels.Add(CreateCard(_playerLblPos[6].X, _playerLblPos[6].Y, card));
                //_playerCardLabels.Add(CreateCard(_playerLblPos[7].X, _playerLblPos[7].Y, card));
                //Controls.AddRange(_playerCardLabels.ToArray());

                //_dealerCardLabels.Add(CreateCard(_dealerLblPos[0].X, _dealerLblPos[0].Y, card));
                //_dealerCardLabels.Add(CreateCard(_dealerLblPos[1].X, _dealerLblPos[1].Y, card));
                //_dealerCardLabels.Add(CreateCard(_dealerLblPos[2].X, _dealerLblPos[2].Y, card));
                //_dealerCardLabels.Add(CreateCard(_dealerLblPos[3].X, _dealerLblPos[3].Y, card));
                //_dealerCardLabels.Add(CreateCard(_dealerLblPos[4].X, _dealerLblPos[4].Y, card));
                //Controls.AddRange(_dealerCardLabels.ToArray());
                #endregion

            }
            catch (ArgumentException error)
            {
                MessageBox.Show(error.Message);
            }
            catch
            {
                //Handles all the other exceptions.
            }
        }


        private void ClearHandLabels()
        {
            lblHand1.Text = "";
            lblHand2.Text = "";
            lblHand3.Text = "";
            lblHand4.Text = "";
        }

        private void btnNewTable_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> names = new List<string>();
                // Might have to remove this later 
                var listArray = names.ToArray();

                if (txtPlayerName1 != null ||
                    txtPlayerName2 != null ||
                    txtPlayerName3 != null ||
                    txtPlayerName4 != null )
                {

                    names.Add(txtPlayerName1.Text);
                    names.Add(txtPlayerName2.Text);
                    names.Add(txtPlayerName3.Text);
                    names.Add(txtPlayerName4.Text);

                    lblPlayerName1.Text = txtPlayerName1.Text;
                    lblPlayerName2.Text = txtPlayerName2.Text;
                    lblPlayerName3.Text = txtPlayerName3.Text;
                    lblPlayerName4.Text = txtPlayerName4.Text;

                    ClearHandLabels();
                    ClearPlayerCardsFromTable();
                    ClearDealerCardsFromTable();
                    btnDrawCard.Enabled = false;
                    btnNewHand.Enabled = true;
                    //btnNewTable.Enabled = false;
                    //if (txtPlayerName1 == null || txtPlayerName2 == null)
                    //{
                    //    btnNewTable.Enabled = false;
                    //}

                    lblwinner.Enabled = false;

                }
                else { MessageBox.Show("Please fill them all"); }
                ClearHandLabels();

            }
            catch (ArgumentException error)
            {
                MessageBox.Show(error.Message);
            }
            catch
            {
                //Handles all the other exceptions.
            }
        }

        private void lblPlayerName1_Click(object sender, EventArgs e)
        {

        }

        private Label CreateCard(int x, int y, Card card)
        {
            var lbl = new Label();
            lbl.Text = card.output;
            lbl.Size = new Size(45, 60);
            lbl.Location = new Point(x, y);
            lbl.BorderStyle = BorderStyle.FixedSingle;
            lbl.Font = new Font("Consolas", 15);
            lbl.TextAlign = ContentAlignment.MiddleCenter;
            lbl.BackColor = Color.White;
            lbl.ForeColor =
                card.Suit.Equals(Suits.Hearts) ||
                card.Suit.Equals(Suits.Diamonds) ?
                Color.Red : Color.Black;
            return lbl;
        }

        private void ClearPlayerCardsFromTable()
        {
            foreach (Label label in _playerCardLabels)
            {
                Controls.Remove(label);
            }
            _playerCardLabels.Clear();
        }

        private void ClearDealerCardsFromTable()
        {
            foreach (Label label in _dealerCardLabels)
            {
                Controls.Remove(label);
            }
        }
    }

}
