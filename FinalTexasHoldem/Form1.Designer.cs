﻿namespace FinalTexasHoldem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPlayerName1 = new System.Windows.Forms.TextBox();
            this.txtPlayerName2 = new System.Windows.Forms.TextBox();
            this.txtPlayerName3 = new System.Windows.Forms.TextBox();
            this.txtPlayerName4 = new System.Windows.Forms.TextBox();
            this.lblHand1 = new System.Windows.Forms.Label();
            this.lblHand2 = new System.Windows.Forms.Label();
            this.lblHand3 = new System.Windows.Forms.Label();
            this.lblHand4 = new System.Windows.Forms.Label();
            this.btnNewTable = new System.Windows.Forms.Button();
            this.lblPlayerName1 = new System.Windows.Forms.Label();
            this.lblPlayerName2 = new System.Windows.Forms.Label();
            this.lblPlayerName3 = new System.Windows.Forms.Label();
            this.lblPlayerName4 = new System.Windows.Forms.Label();
            this.btnNewHand = new System.Windows.Forms.Button();
            this.btnDrawCard = new System.Windows.Forms.Button();
            this.lblwinner = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Player 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Player 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Player 3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Player 4";
            // 
            // txtPlayerName1
            // 
            this.txtPlayerName1.Location = new System.Drawing.Point(66, 0);
            this.txtPlayerName1.Name = "txtPlayerName1";
            this.txtPlayerName1.Size = new System.Drawing.Size(100, 22);
            this.txtPlayerName1.TabIndex = 4;
            // 
            // txtPlayerName2
            // 
            this.txtPlayerName2.Location = new System.Drawing.Point(66, 27);
            this.txtPlayerName2.Name = "txtPlayerName2";
            this.txtPlayerName2.Size = new System.Drawing.Size(100, 22);
            this.txtPlayerName2.TabIndex = 5;
            // 
            // txtPlayerName3
            // 
            this.txtPlayerName3.Location = new System.Drawing.Point(66, 55);
            this.txtPlayerName3.Name = "txtPlayerName3";
            this.txtPlayerName3.Size = new System.Drawing.Size(100, 22);
            this.txtPlayerName3.TabIndex = 6;
            // 
            // txtPlayerName4
            // 
            this.txtPlayerName4.Location = new System.Drawing.Point(66, 83);
            this.txtPlayerName4.Name = "txtPlayerName4";
            this.txtPlayerName4.Size = new System.Drawing.Size(100, 22);
            this.txtPlayerName4.TabIndex = 7;
            // 
            // lblHand1
            // 
            this.lblHand1.AutoSize = true;
            this.lblHand1.Location = new System.Drawing.Point(182, 3);
            this.lblHand1.Name = "lblHand1";
            this.lblHand1.Size = new System.Drawing.Size(0, 17);
            this.lblHand1.TabIndex = 8;
            // 
            // lblHand2
            // 
            this.lblHand2.AutoSize = true;
            this.lblHand2.Location = new System.Drawing.Point(182, 30);
            this.lblHand2.Name = "lblHand2";
            this.lblHand2.Size = new System.Drawing.Size(0, 17);
            this.lblHand2.TabIndex = 9;
            // 
            // lblHand3
            // 
            this.lblHand3.AutoSize = true;
            this.lblHand3.Location = new System.Drawing.Point(172, 58);
            this.lblHand3.Name = "lblHand3";
            this.lblHand3.Size = new System.Drawing.Size(0, 17);
            this.lblHand3.TabIndex = 10;
            // 
            // lblHand4
            // 
            this.lblHand4.AutoSize = true;
            this.lblHand4.Location = new System.Drawing.Point(172, 86);
            this.lblHand4.Name = "lblHand4";
            this.lblHand4.Size = new System.Drawing.Size(0, 17);
            this.lblHand4.TabIndex = 11;
            // 
            // btnNewTable
            // 
            this.btnNewTable.Location = new System.Drawing.Point(3, 111);
            this.btnNewTable.Name = "btnNewTable";
            this.btnNewTable.Size = new System.Drawing.Size(99, 23);
            this.btnNewTable.TabIndex = 12;
            this.btnNewTable.Text = "New Table";
            this.btnNewTable.UseVisualStyleBackColor = true;
            this.btnNewTable.Click += new System.EventHandler(this.btnNewTable_Click);
            // 
            // lblPlayerName1
            // 
            this.lblPlayerName1.Location = new System.Drawing.Point(785, 281);
            this.lblPlayerName1.Name = "lblPlayerName1";
            this.lblPlayerName1.Size = new System.Drawing.Size(85, 24);
            this.lblPlayerName1.TabIndex = 13;
            this.lblPlayerName1.Text = "Player 1";
            this.lblPlayerName1.Click += new System.EventHandler(this.lblPlayerName1_Click);
            // 
            // lblPlayerName2
            // 
            this.lblPlayerName2.Location = new System.Drawing.Point(529, 470);
            this.lblPlayerName2.Name = "lblPlayerName2";
            this.lblPlayerName2.Size = new System.Drawing.Size(100, 23);
            this.lblPlayerName2.TabIndex = 14;
            this.lblPlayerName2.Text = "Player 2";
            // 
            // lblPlayerName3
            // 
            this.lblPlayerName3.Location = new System.Drawing.Point(327, 470);
            this.lblPlayerName3.Name = "lblPlayerName3";
            this.lblPlayerName3.Size = new System.Drawing.Size(100, 23);
            this.lblPlayerName3.TabIndex = 15;
            this.lblPlayerName3.Text = "Player 3";
            // 
            // lblPlayerName4
            // 
            this.lblPlayerName4.Location = new System.Drawing.Point(2, 282);
            this.lblPlayerName4.Name = "lblPlayerName4";
            this.lblPlayerName4.Size = new System.Drawing.Size(100, 23);
            this.lblPlayerName4.TabIndex = 16;
            this.lblPlayerName4.Text = "Player 4";
            // 
            // btnNewHand
            // 
            this.btnNewHand.Enabled = false;
            this.btnNewHand.Location = new System.Drawing.Point(330, 86);
            this.btnNewHand.Name = "btnNewHand";
            this.btnNewHand.Size = new System.Drawing.Size(91, 23);
            this.btnNewHand.TabIndex = 17;
            this.btnNewHand.Text = "New Hand";
            this.btnNewHand.UseVisualStyleBackColor = true;
            // 
            // btnDrawCard
            // 
            this.btnDrawCard.Enabled = false;
            this.btnDrawCard.Location = new System.Drawing.Point(427, 86);
            this.btnDrawCard.Name = "btnDrawCard";
            this.btnDrawCard.Size = new System.Drawing.Size(101, 23);
            this.btnDrawCard.TabIndex = 18;
            this.btnDrawCard.Text = "Draw Card";
            this.btnDrawCard.UseVisualStyleBackColor = true;
            // 
            // lblwinner
            // 
            this.lblwinner.AutoSize = true;
            this.lblwinner.Location = new System.Drawing.Point(392, 55);
            this.lblwinner.Name = "lblwinner";
            this.lblwinner.Size = new System.Drawing.Size(56, 17);
            this.lblwinner.TabIndex = 19;
            this.lblwinner.Text = "Winner!";
            this.lblwinner.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(882, 583);
            this.Controls.Add(this.lblwinner);
            this.Controls.Add(this.btnDrawCard);
            this.Controls.Add(this.btnNewHand);
            this.Controls.Add(this.lblPlayerName4);
            this.Controls.Add(this.lblPlayerName3);
            this.Controls.Add(this.lblPlayerName2);
            this.Controls.Add(this.lblPlayerName1);
            this.Controls.Add(this.btnNewTable);
            this.Controls.Add(this.lblHand4);
            this.Controls.Add(this.lblHand3);
            this.Controls.Add(this.lblHand2);
            this.Controls.Add(this.lblHand1);
            this.Controls.Add(this.txtPlayerName4);
            this.Controls.Add(this.txtPlayerName3);
            this.Controls.Add(this.txtPlayerName2);
            this.Controls.Add(this.txtPlayerName1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPlayerName1;
        private System.Windows.Forms.TextBox txtPlayerName2;
        private System.Windows.Forms.TextBox txtPlayerName3;
        private System.Windows.Forms.TextBox txtPlayerName4;
        private System.Windows.Forms.Label lblHand1;
        private System.Windows.Forms.Label lblHand2;
        private System.Windows.Forms.Label lblHand3;
        private System.Windows.Forms.Label lblHand4;
        private System.Windows.Forms.Button btnNewTable;
        private System.Windows.Forms.Label lblPlayerName1;
        private System.Windows.Forms.Label lblPlayerName2;
        private System.Windows.Forms.Label lblPlayerName3;
        private System.Windows.Forms.Label lblPlayerName4;
        private System.Windows.Forms.Button btnNewHand;
        private System.Windows.Forms.Button btnDrawCard;
        private System.Windows.Forms.Label lblwinner;
    }
}

